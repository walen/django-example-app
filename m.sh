#!/bin/bash
if [ ! -d virtualenv ] ; then
  ./_create_virtualenv
fi
. virtualenv/bin/activate
python manage.py "$@"