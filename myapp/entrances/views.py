from django.http import HttpResponse
from myapp.utils.with_template import with_template


def hello(request):
    return HttpResponse("Hello world", content_type="text/plain")


@with_template
def home(request):
    in_home = True
    return locals()
