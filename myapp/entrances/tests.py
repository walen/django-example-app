from django.test import TestCase, Client
from django.core.urlresolvers import reverse


class EntrancesTestCase(TestCase):

    def setUp(self):
        self.client = Client()

    def test_home(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
