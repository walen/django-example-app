# more info about urls.py:
# - ?
# - static files: https://docs.djangoproject.com/en/1.7/howto/static-files/
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static

urlpatterns = patterns('',
    url(r'^$', 'myapp.entrances.views.home', name='home'),
    url(r'^hello/?$', 'myapp.entrances.views.hello', name='hello'),
    url(r'^library/', include('myapp.library.urls')),
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
