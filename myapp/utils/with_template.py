"""with_template decorator: a quick replacement for render_to_response.

As described here:
http://marcinkaszynski.com/blog/index.php/2008/10/01/skroty-render_to_response/
"""

import re

from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
try:
    # functools appeared in Python 2.5
    from functools import update_wrapper
except ImportError: #pragma: no cover
    # luckily for us, Django already backported it
    from django.utils.functional import update_wrapper

def with_template(arg):
    """
    A view decorator that handles rendering.

    If the view returns a HttpResponse, it is passed intact; otherwise
    the returned value is passed as dictionary to render_to_response.

    Usage samples:

    @with_template
    def view_func(request, ...):
        return ...

    @with_template('custom/template/name.html')
    def other_view_func(request, ...):
        return ...
    """

    class TheWrapper(object):
        def __init__(self, default_template_name):
            self.default_template_name = default_template_name

        def __call__(self, func):
            def decorated_func(request, *args, **kwargs):
                extra_context = kwargs.pop('extra_context', {})
                dictionary = {}
                ret = func(request, *args, **kwargs)
                if isinstance(ret, HttpResponse):
                    return ret
                dictionary.update(ret)
                dictionary.update(extra_context)
                return render_to_response(dictionary.get('template_name',
                                                         self.default_template_name),
                                          context_instance=RequestContext(request),
                                          dictionary=dictionary)
            update_wrapper(decorated_func, func)
            return decorated_func

    if not callable(arg):
        return TheWrapper(arg)
    else:
        app_name = re.search('([^.]+)[.]views', arg.__module__).group(1)
        default_template_name = ''.join([app_name, '/', arg.__name__, '.html'])
        return TheWrapper(default_template_name)(arg)

