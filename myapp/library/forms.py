from django import forms
from myapp.library.models import Book


class BookForm(forms.Form):
    author = forms.CharField(label='Author', max_length=100)
    title = forms.CharField(label='Title', max_length=100)

    def save(self):
        title = self.cleaned_data['title']
        author = self.cleaned_data['author']
        return Book.objects.create(title=title, author=author)
