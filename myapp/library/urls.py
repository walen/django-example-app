from django.conf.urls import patterns, url

from myapp.library import views

urlpatterns = patterns('',
    url(r'^$', views.library_home, name='library_home'),
    url(r'^book/list/$', views.books_list, name='books_list'),
    url(r'^book/info/(?P<id>[0-9]+)/$', views.book_info, name='book_info'),
    url(r'^book/create/', views.book_create, name='book_create'),
    url(r'^book/list/json/$', views.books_list_json, name='books_list_json'),
    url(r'^person/list/$', views.persons_list, name='persons_list'),
    url(r'^person/list/json/$', views.persons_list_json, name='persons_list_json'),
    url(r'^borrow/list/$', views.borrowed_list, name='borrowed_list'),
    url(r'^borrow/list/json/$', views.borrowed_list_json, name='borrowed_list_json'),
)