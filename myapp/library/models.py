from django.db import models


class Person(models.Model):
    pesel = models.CharField(unique=True,max_length=11)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

    def __str__(self):
        return self.pesel+": "+self.first_name+" "+self.last_name


class Book(models.Model):
    title = models.CharField(max_length=128)
    author = models.CharField(max_length=128)

    def __str__(self):
        return self.author+": "+self.title

    @property
    def available(self):
        return not Borrow.objects.filter(book=self, returned_at=None).exists()

class Borrow(models.Model):
    person = models.ForeignKey(Person)
    book = models.ForeignKey(Book)
    borrowed_at = models.DateTimeField()
    returned_at = models.DateTimeField(null=True)
