import simplejson
from django.core import urlresolvers
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from myapp.utils.with_template import with_template
from myapp.library.models import Book, Person, Borrow
from myapp.library.forms import BookForm


@with_template
def library_home(request):
    in_library = True
    return locals()


def _qs_to_dict(qs, fields):
    res = []
    for row in qs.values():
        res.append(dict((f, row[f]) for f in fields))
    return res


@with_template
def books_list(request):
    in_library = True
    return locals()


@with_template
def book_info(request, id):
    book = get_object_or_404(Book.objects.all(), id=id)
    in_library = True
    return locals()


@with_template
def book_create(request):
    if request.method == 'POST':
        form = BookForm(request.POST)
        if form.is_valid():
            book = form.save()
            return HttpResponseRedirect(urlresolvers.reverse("book_info", kwargs={"id": book.id}))
    else:
        form = BookForm()
    in_library = True
    return locals()


def books_list_json(request):
    qs = Book.objects.all()
    qs = qs.order_by('id')
    res = _qs_to_dict(qs, ['id', 'title', 'author'])
    return HttpResponse(simplejson.dumps(res), content_type="text/plain")


# http://www.dailywritingtips.com/people-versus-persons/
@with_template
def persons_list(request):
    in_library = True
    return locals()


def persons_list_json(request):
    qs = Person.objects.all()
    qs = qs.order_by('id')
    res = _qs_to_dict(qs, ['id', 'pesel', 'first_name', 'last_name'])
    return HttpResponse(simplejson.dumps(res), content_type="text/plain")


@with_template
def borrowed_list(request):
    in_library = True
    return locals()


def borrowed_list_json(request):
    qs = Borrow.objects.all()
    qs = qs.order_by('id')
    res = _qs_to_dict(qs, ['id', 'book_id', 'person_id', 'borrowed_at', 'returned_at'])
    for f in ['borrowed_at', 'returned_at']:
        for row in res:
            if row[f] is not None:
                row[f] = row[f].strftime("%Y-%m-%d %H:%M:%S")
    return HttpResponse(simplejson.dumps(res), content_type="text/plain")
