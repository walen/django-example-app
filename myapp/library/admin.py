from django.contrib import admin
###
from myapp.library.models import Person, Book


class PersonAdmin(admin.ModelAdmin):
    list_display = ['id', 'pesel', 'first_name', 'last_name']


class BookAdmin(admin.ModelAdmin):
    list_display = ['id', 'author', 'title']


admin.site.register(Person, PersonAdmin)
admin.site.register(Book, BookAdmin)
