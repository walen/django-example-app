from django.test import TestCase, Client
from django.core.urlresolvers import reverse


class LibraryTestCase(TestCase):

    def setUp(self):
        self.client = Client()

    def test_library_home(self):
        response = self.client.get(reverse('library_home'))
        self.assertEqual(response.status_code, 200)
